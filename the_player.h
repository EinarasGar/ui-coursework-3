//
// Created by twak on 11/11/2019.
//

#ifndef CW2_THE_PLAYER_H
#define CW2_THE_PLAYER_H


#include <QApplication>
#include <QMediaPlayer>
#include "the_button.h"
#include <vector>
#include <QTimer>
#include "QSlider"

using namespace std;

class ThePlayer : public QMediaPlayer {

Q_OBJECT

private:
    vector<TheButtonInfo>* infos;
    vector<TheButton*>* buttons;
    QTimer* mTimer;
    long updateCount = 0;
    QUrl* currentUrl;
    QSlider* slider = nullptr;

public:
    ThePlayer() : QMediaPlayer(NULL) {
        setVolume(0); // be slightly less annoying
        connect (this, SIGNAL (stateChanged(QMediaPlayer::State)), this, SLOT (playStateChanged(QMediaPlayer::State)) );
        connect (this, SIGNAL (durationChanged(qint64)), this, SLOT (durationChanged(qint64)) );
        connect (this, SIGNAL (positionChanged(qint64)), this, SLOT (positionChanged(qint64)) );

        //mTimer = new QTimer(NULL);
        //mTimer->setInterval(5000); // 1000ms is one second between ...
        //mTimer->start();
       // connect( mTimer, SIGNAL (timeout()), SLOT ( shuffle() ) ); // ...running shuffle method
    }

    // all buttons have been setup, store pointers here
    void setContent(vector<TheButton*>* b, vector<TheButtonInfo>* i);
    void SetSlider(QSlider * Slider);

private slots:

    // change the image and video for one button every one second
    void shuffle();

    void playStateChanged (QMediaPlayer::State ms);
    void durationChanged(qint64 duration);
    void positionChanged(qint64 position);
    void setMuted();


    void playPause();
    void forward();
    void backwards();

public slots:

    // start playing this ButtonInfo
    void jumpTo (TheButtonInfo* button);
};

#endif //CW2_THE_PLAYER_H
