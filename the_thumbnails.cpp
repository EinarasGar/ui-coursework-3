#include "the_thumbnails.h"
#include "QVBoxLayout"
#include "the_button.h"
#include "qdir.h"
#include "qfile.h"
#include "qimagereader.h"
#include "qdiriterator.h"
#include "QString"
#include <QtAlgorithms>

using namespace std;

void TheThumbnails::SetPlayer(ThePlayer* Player){
    player = Player;
}

// read in videos and thumbnails to this directory
vector<TheButtonInfo> TheThumbnails::getInfoIn (string loc) {

    vector<TheButtonInfo> out =  vector<TheButtonInfo>();
    QDir dir(QString::fromStdString(loc) );
    QDirIterator it(dir);

    while (it.hasNext()) { // for all files

        QString f = it.next();

            if (f.contains("."))

#if defined(_WIN32)
            if (f.contains(".wmv"))  { // windows
#else
            if (f.contains(".mp4") || f.contains("MOV"))  { // mac/linux
#endif

            QString thumb = f.left( f .length() - 4) +".png";
            if (QFile(thumb).exists()) { // if a png thumbnail exists
                QImageReader *imageReader = new QImageReader(thumb);
                    QImage sprite = imageReader->read(); // read the thumbnail
                    if (!sprite.isNull()) {
                        QIcon* ico = new QIcon(QPixmap::fromImage(sprite)); // voodoo to create an icon for the button
                        QUrl* url = new QUrl(QUrl::fromLocalFile( f )); // convert the file location to a generic url
                        QFileInfo* fileInfo = new QFileInfo(f);
                        QString fileName = f.split("/").last().split(".").first();
                        QDateTime date = fileInfo->birthTime();
                        out . push_back(TheButtonInfo( url , ico, date, fileName) ); // add to the output list
                    }
                    else
                        qDebug() << "warning: skipping video because I couldn't process thumbnail " << thumb << endl;
            }
            else
                qDebug() << "warning: skipping video because I couldn't find thumbnail " << thumb << endl;
        }
    }

    return out;
}

bool dtcomp(TheButtonInfo left, TheButtonInfo right) {
  return left.date < right.date;
}

bool dtcomp2(TheButtonInfo left, TheButtonInfo right) {
  return left.date > right.date;
}

bool ncomp(TheButtonInfo left, TheButtonInfo right) {
  return left.name > right.name;
}

bool ncomp2(TheButtonInfo left, TheButtonInfo right) {
  return left.name < right.name;
}


void TheThumbnails::ChangeThumbnails(string loc){
    curLoc = loc;
    videos.clear();
    buttons.clear();
    videos = getInfoIn(loc);
    QWidget *buttonWidget = new QWidget();
    // a list of the buttons
    // the buttons are arranged horizontally
    QVBoxLayout *layout = new QVBoxLayout();
    buttonWidget->setLayout(layout);

    if(sort == 0) qDebug() << "Not sorting";
    if(sort == 1) {qDebug() << "date ascending"; qSort(videos.begin(), videos.end(), dtcomp);}
    if(sort == 2) {qDebug() << "date descending";qSort(videos.begin(), videos.end(), dtcomp2);}
    if(sort == 3) {qDebug() << "name ascending";qSort(videos.begin(), videos.end(), ncomp);}
    if(sort == 4) {qDebug() << "name descending";qSort(videos.begin(), videos.end(), ncomp2);}




    // create the four buttons
    for ( int i = 0; i < videos.size(); i++ ) {
        TheButton *button = new TheButton(buttonWidget);
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), player, SLOT (jumpTo(TheButtonInfo* ))); // when clicked, tell the player to play.
        button->setMinimumSize(200,100);
        buttons.push_back(button);

        layout->addWidget(button);
        button->init(&videos.at(i));
    }

    // tell the player what buttons and videos are available
    if(videos.size() > 0)
        player->setContent(&buttons, & videos);

    buttonWidget->setMinimumHeight(videos.size()*120);
    scroll->setWidget(buttonWidget);
}

void TheThumbnails::SetStandardItemModel(QStandardItemModel* Model){
    model = Model;
}

void TheThumbnails::treeClicked(const QModelIndex &index){
    QStandardItem *item = model->itemFromIndex(index);
    QString dir = item->accessibleText();
    ChangeThumbnails(dir.toUtf8().constData());
}

void TheThumbnails::sortByDate(){
    if(sort == 1)
        sort = 2;
    else
        sort = 1;
    ChangeThumbnails(curLoc);
}

void TheThumbnails::sortByName(){
    if(sort == 3)
        sort = 4;
    else
        sort = 3;
    ChangeThumbnails(curLoc);
}
