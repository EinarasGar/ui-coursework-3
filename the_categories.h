#ifndef THE_CATEGORIES_H
#define THE_CATEGORIES_H

#include <QApplication>
#include <QMediaPlayer>
#include "the_button.h"
#include <vector>
#include <QTimer>
#include "QTreeView"
#include "the_thumbnails.h"

using namespace std;

class TheCategories : public QTreeView {

Q_OBJECT

private:
    TheThumbnails* thumbnails;
    string mainLoc;
    QStandardItemModel* model;
    QStringList categoryList;

public:
    TheCategories(string loc) : QTreeView() {
        mainLoc = loc;
        this->setHeaderHidden(true);
        this->setMinimumWidth(200);
    }

    void SetThumbnailObject(TheThumbnails* Thumbnails);
    void UpdateCategories();

private:
    vector<QString> GetCategories(string loc);

public slots:
    void AddCategorySlot();
    void AddVideoSlot();

};


#endif // THE_CATEGORIES_H
