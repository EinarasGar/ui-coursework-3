//
// Created by twak on 11/11/2019.
//

#include "the_player.h"

using namespace std;

// all buttons have been setup, store pointers here
void ThePlayer::setContent(std::vector<TheButton*>* b, std::vector<TheButtonInfo>* i) {
    buttons = b;
    infos = i;
    currentUrl = buttons -> at(0) -> info ->url;
    jumpTo(buttons -> at(0) -> info);
}

// change the image and video for one button every one second
void ThePlayer::shuffle() {
    TheButtonInfo* i = & infos -> at (rand() % infos->size() );
//        setMedia(*i->url);
    buttons -> at( updateCount++ % buttons->size() ) -> init( i );
}

void ThePlayer::playStateChanged (QMediaPlayer::State ms) {
    switch (ms) {
        case QMediaPlayer::State::StoppedState:
            play(); // starting playing again...
            break;
    default:
        break;
    }
}

void ThePlayer::jumpTo (TheButtonInfo* button) {
    currentUrl = button -> url;
    setMedia( * button -> url);
    play();
}

void ThePlayer::SetSlider(QSlider * Slider){
    slider = Slider;
}

void ThePlayer::playPause(){
    if(this->state() == PausedState){
        this->play();
    } else {
        this->pause();
    }
}

void ThePlayer::forward(){
    for(int i = 0; i < buttons->size(); i++){
        if(buttons->at(i)->info->url == currentUrl){
            if(i+1 < buttons->size()){
                jumpTo(buttons -> at(i+1) -> info);
                return;
            }
        }
    }
}

void ThePlayer::backwards(){
    for(int i = 0; i < buttons->size(); i++){
        if(buttons->at(i)->info->url == currentUrl){
            if(i > 0){
                jumpTo(buttons -> at(i-1) -> info);
                return;
            }
        }
    }
}

void ThePlayer::durationChanged(qint64 duration){
    if(slider != nullptr)
        slider->setMaximum(duration);
}
void ThePlayer::setMuted(){
    if(this->isMuted()){
        this->setVolume(80);
    } else {

        this->setVolume(0);
    }
}


void ThePlayer::positionChanged(qint64 position){
    if(slider != nullptr)
        slider->setValue(position);
}
