#ifndef THE_THUMBNAILS_H
#define THE_THUMBNAILS_H


#include <QApplication>
#include <QMediaPlayer>
#include "the_button.h"
#include "the_player.h"
#include <vector>
#include <QTimer>
#include <QScrollArea>
#include "QStandardItemModel"
#include "vector"
#include "QVBoxLayout"

using namespace std;

class TheThumbnails : public QWidget {

Q_OBJECT

private:
    ThePlayer* player;
    vector<TheButton*> buttons;
    vector<TheButtonInfo> videos;
    QStandardItemModel* model;
    QScrollArea* scroll;
    QVBoxLayout* vbox;
    QHBoxLayout* hbox;
    QWidget* hBoxWidget;
    int sort = 0;
    string curLoc;

public:
    TheThumbnails() : QWidget() {
        //Box for housing buttons and scrollarea
        vbox = new QVBoxLayout();

        //Box for housing just buttons
        hBoxWidget = new QWidget();
        hbox = new QHBoxLayout();
        QPushButton* button1 = new QPushButton("Sort by name");
        QPushButton* button2 = new QPushButton("Sort by date");
        button1->connect(button1, SIGNAL(released()),this, SLOT(sortByName()));
        button2->connect(button2, SIGNAL(released()),this, SLOT(sortByDate()));
        hbox->addWidget(button1);
        hbox->addWidget(button2);
        hbox->setSpacing(0);
        hBoxWidget->setLayout(hbox);

        scroll = new QScrollArea();
        scroll->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        scroll->setMinimumWidth(220);
        scroll->ensureWidgetVisible(0,0);
        scroll->setWidgetResizable(true);
        vbox->addWidget(hBoxWidget);
        vbox->addWidget(scroll);
        vbox->setSpacing(0);

        this->setLayout(vbox);

    }

    void SetPlayer(ThePlayer* player);
    void ChangeThumbnails(string loc);
    void SetStandardItemModel(QStandardItemModel* model);

private:
    vector<TheButtonInfo> getInfoIn (string loc);

public slots:
    void treeClicked(const QModelIndex &index);
    void sortByDate();
    void sortByName();

};


#endif // THE_THUMBNAILS_H
