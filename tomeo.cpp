/*
 *
 *    ______
 *   /_  __/___  ____ ___  ___  ____
 *    / / / __ \/ __ `__ \/ _ \/ __ \
 *   / / / /_/ / / / / / /  __/ /_/ /
 *  /_/  \____/_/ /_/ /_/\___/\____/
 *              video for sports enthusiasts...
 *
 *  2811 cw3 : twak
 */

#include <iostream>
#include <QApplication>
#include <QtMultimediaWidgets/QVideoWidget>
#include <QMediaPlaylist>
#include <string>
#include <vector>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QHBoxLayout>
#include <QtCore/QFileInfo>
#include <QtWidgets/QFileIconProvider>
#include <QDesktopServices>
#include <QImageReader>
#include <QMessageBox>
#include <QtCore/QDir>
#include <QtCore/QDirIterator>
#include "the_player.h"
#include "the_button.h"
#include "the_thumbnails.h"
#include "QScrollArea"
#include "qlabel.h"
#include "QSlider"
#include "qtreeview.h"
#include "QStandardItemModel"
#include "QStandardItem"
#include "the_categories.h"

using namespace std;

vector<QString> getCategories(string loc){
    vector<QString> out =  vector<QString>();
    QDir dir(QString::fromStdString(loc) );
    QDirIterator it(dir);
    while(it.hasNext()){
        QString f = it.next();
        if(!f.contains("."))
            out.push_back(f);
    }
    return out;
}


int main(int argc, char *argv[]) {

    // let's just check that Qt is operational first
    qDebug() << "Qt version: " << QT_VERSION_STR << endl;

    // create the Qt Application
    QApplication app(argc, argv);




    /*if (videos.size() == 0) {

        const int result = QMessageBox::question(
                    NULL,
                    QString("Tomeo"),
                    QString("no videos found! download, unzip, and add command line argument to \"quoted\" file location. Download videos from Tom's OneDrive?"),
                    QMessageBox::Yes |
                    QMessageBox::No );

        switch( result )
        {
        case QMessageBox::Yes:
          QDesktopServices::openUrl(QUrl("https://leeds365-my.sharepoint.com/:u:/g/personal/scstke_leeds_ac_uk/EcGntcL-K3JOiaZF4T_uaA4BHn6USbq2E55kF_BTfdpPag?e=n1qfuN"));
          break;
        default:
            break;
        }
        exit(-1);
    }*/

    // the widget that will show the video
    QVideoWidget *videoWidget = new QVideoWidget;

    // the QMediaPlayer which controls the playback
    ThePlayer *player = new ThePlayer;
    player->setVideoOutput(videoWidget);

    // create the main window and layout
    QWidget window;
    QVBoxLayout *top = new QVBoxLayout();
    window.setLayout(top);
    window.setWindowTitle("tomeo");
    window.setMinimumSize(800, 680);

    TheThumbnails* scrollArea2 = new TheThumbnails();
    scrollArea2->SetPlayer(player);
    scrollArea2->ChangeThumbnails(string(argv[1]));

    QWidget *toolBarWidget = new QWidget();
    QHBoxLayout *toolBar= new QHBoxLayout();
    QLabel* label = new QLabel("Tomeo!");

    QFont font = label->font();
    font.setPointSize(22);
    font.setBold(true);
    label->setFont(font);

    QPushButton *addVideoButton = new QPushButton("Add video");
    QPushButton *addCategoryButton = new QPushButton("Add Category");
    addVideoButton->setMaximumSize(100,100);
    addCategoryButton->setMaximumSize(100,100);


    toolBar->addWidget(label);
    toolBar->addWidget(addCategoryButton);
    toolBar->addWidget(addVideoButton);

    toolBarWidget->setLayout(toolBar);

    // add the video and the buttons to the top level widget
    top->addWidget(toolBarWidget);

    QWidget *videoWithThumbnailsWidget = new QWidget();
    QHBoxLayout *videoWithThumbnails= new QHBoxLayout();

    TheCategories* tree2 = new TheCategories(string(argv[1]));
    tree2->SetThumbnailObject(scrollArea2);
    tree2->UpdateCategories();
    addCategoryButton->connect(addCategoryButton, SIGNAL(released()), tree2, SLOT (AddCategorySlot()));
    addVideoButton->connect(addVideoButton, SIGNAL(released()), tree2, SLOT (AddVideoSlot()));

    videoWithThumbnails->addWidget(tree2);
    videoWithThumbnails->addWidget(scrollArea2);

    QWidget *videoWithControlsWidget = new QWidget();
    QVBoxLayout *videoWithControls = new QVBoxLayout();
    videoWithControls->addWidget(videoWidget);

    QWidget *playbackButtonsWidget = new QWidget();
    QHBoxLayout *playbackButtons = new QHBoxLayout();

    QPushButton *mute = new QPushButton("Mute");
    QPushButton *playButton = new QPushButton("Play");
    QPushButton *previousButton = new QPushButton("Previous");
    QPushButton *nextButton = new QPushButton("Next");
    QSlider *videoProgressBar = new QSlider(Qt::Orientation::Horizontal);

    player->SetSlider(videoProgressBar);

    playButton->connect(playButton, SIGNAL(released()), player, SLOT (playPause()));
    nextButton->connect(nextButton, SIGNAL(released()), player, SLOT (forward()));
    previousButton->connect(previousButton, SIGNAL(released()), player, SLOT (backwards()));
    mute->connect(mute,SIGNAL(released()),player,SLOT(setMuted()));

    videoProgressBar->setMinimumWidth(200);
    playbackButtons->addWidget(playButton);
    playbackButtons->addWidget(mute);
    playbackButtons->addWidget(previousButton);
    playbackButtons->addWidget(nextButton);
    playbackButtons->addWidget(videoProgressBar);

    playbackButtonsWidget->setLayout(playbackButtons);
    videoWithControls->addWidget(playbackButtonsWidget);

    videoWithControlsWidget->setLayout(videoWithControls);


    videoWithThumbnails->addWidget(videoWithControlsWidget);
    videoWithThumbnailsWidget->setLayout(videoWithThumbnails);
    top->addWidget(videoWithThumbnailsWidget);

    // showtime!
    window.show();

    // wait for the app to terminate
    return app.exec();
}
