#include "the_categories.h"
#include "QDir"
#include "qdiriterator.h"
#include "QInputDialog"
#include "QFileDialog"
#include "QFile"

using namespace std;

void TheCategories::SetThumbnailObject(TheThumbnails* Thumbnails){
    thumbnails = Thumbnails;
}

vector<QString> TheCategories::GetCategories(string loc){
    vector<QString> out =  vector<QString>();
    QDir dir(QString::fromStdString(loc) );
    QDirIterator it(dir);
    while(it.hasNext()){
        QString f = it.next();
        if(!f.contains("."))
            out.push_back(f);
    }
    return out;
}

void TheCategories::UpdateCategories(){
    model = new QStandardItemModel();
    categoryList.clear();

    vector<QString> categories = GetCategories(mainLoc);
    for(int i = 0; i < categories.size(); i++){
        QString category = categories[i];
        QStringList splitted = category.split("/");
        QString name = splitted.last();
        QStandardItem* item = new QStandardItem(name);
        item->setAccessibleText(category);
        item->setEditable(false);
        QFont categoryFont = item->font();
        categoryFont.setPointSize(15);
        categoryFont.setBold(true);
        item->setFont(categoryFont);
        model->appendRow(item);
        categoryList.append(name);

        vector<QString> subCategories = GetCategories(category.toUtf8().constData());
        for(int j = 0; j < subCategories.size(); j++){
            QString subCategory = subCategories[j];
            QStringList subSplitted = subCategory.split("/");
            QString subName = subSplitted.last();
            QStandardItem* subItem = new QStandardItem(subName);
            subItem->setAccessibleText(subCategory);
            subItem->setEditable(false);
            QFont subCatergoryFont = subItem->font();
            subCatergoryFont.setPointSize(10);
            subItem->setFont(subCatergoryFont);
            item->appendRow(subItem);

            QString fullSubCategoryName;
            fullSubCategoryName.append(name);
            fullSubCategoryName.append("/");
            fullSubCategoryName.append(subName);
            categoryList.append(fullSubCategoryName);
        }
    }

    thumbnails->SetStandardItemModel(model);
    this->connect(this, &QTreeView::clicked,thumbnails,&TheThumbnails::treeClicked);
    this->setModel(model);
}

void TheCategories::AddCategorySlot(){
    bool ok;
    QString text = QInputDialog::getText(this, tr("Add category"),
                                            tr("Enter category name. To add subcategories use \"/\" after main category name."), QLineEdit::Normal,
                                            "", &ok);
    if (ok && !text.isEmpty()){
        QString finalDirl = QString::fromStdString(mainLoc);
        finalDirl.append("/");
        finalDirl.append(text);
        qDebug()<<finalDirl << endl;
        QDir().mkdir(finalDirl);
        this->disconnect();
        UpdateCategories();
    }

}

void TheCategories::AddVideoSlot(){
    QFileDialog dialog(this);
    dialog.setWindowTitle("Select video");
    dialog.setFileMode(QFileDialog::ExistingFile);
    dialog.setNameFilter(tr("Video (*.wmv *.mp4 *.MOV)"));
    dialog.setViewMode(QFileDialog::Detail);
    QStringList fileNames;
    if (dialog.exec()){
        fileNames = dialog.selectedFiles();

        bool ok;
        QString text = QInputDialog::getItem(this, tr("Add video."),
                                             tr("Select category:"), categoryList, 0, false, &ok);

        if (ok && !text.isEmpty()){
            QString videoLocWithoutExtension = fileNames.first().split(".").first();
            QString videoName = videoLocWithoutExtension.split("/").last();
            QString destinationLocWithoutExtension = QString::fromStdString(mainLoc);
            destinationLocWithoutExtension.append("/");
            destinationLocWithoutExtension.append(text);
            destinationLocWithoutExtension.append("/");
            destinationLocWithoutExtension.append(videoName);

            if(QFile::exists(videoLocWithoutExtension+".png")){
                QFile::copy(videoLocWithoutExtension+".png", destinationLocWithoutExtension+".png");
            }
            if(QFile::exists(videoLocWithoutExtension+".wmv")){
                QFile::copy(videoLocWithoutExtension+".wmv", destinationLocWithoutExtension+".wmv");
            }
            if(QFile::exists(videoLocWithoutExtension+".MOV")){
                QFile::copy(videoLocWithoutExtension+".MOV", destinationLocWithoutExtension+".MOV");
            }
            if(QFile::exists(videoLocWithoutExtension+".mp4")){
                QFile::copy(videoLocWithoutExtension+".mp4", destinationLocWithoutExtension+".mp4");
            }
        }
    }
}
